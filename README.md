<h1>Calculadora Cientifica Avanzada</h1>


!--- Primer Hito ---!

Integrantes:
Sebastian Orellana,Enrique Manzano,Nicolas Sepulveda,Tomás Salinas

Docente:
Patricio Olivares

Roles:
- Tomás Salinas: Director Creativo (Ayuda con ideas de codigo, gitlab y readme).
- Sebastian Orellana: Director Audiovisual(creacion de video, powers y gitlab).
- Enrique Manzano: Organizador(Organizar gitlab, ayudar con codigo).
- Nicolas Sepulveda: Desarrollador Senior (Desarrollador principal, Jefe de proyecto).

1. A cada integrante del grupo se le asignó una operación matemática básica del código(suma,resta,división,multiplicación) y cada uno arregló los errores que le surgieron. Tambien agregamos funciones geometricas (Pitagoras, Pendiente).

1. Despues de una semana de repartir las tares, avanzámos en el código, mientras un  companero juntó todo para que funcionara y revisando detalles de este como mejorando lineas y dejandolo mas compacto, igual ayudandonos entre nosotros a mejorar.

1. Al tener las 4 funciones principales, fuimos agregando pocas a medida que ibamos pensando en como podiamos mejorar nuestra Calculadora para todos,como por ejemplo la funcion de calcular la pendiente con 2 puntos en un plano.

1. Poco a poco se fue puliendo el codigo , agregando funciones para hacer más didactico todo. Empezamos a darnos cuenta que todo era muy engoroso para el usario asi que dicidimos hacer un menu para la comodidad de quien lo use.



![fotodiagrama](Diagrama_de_componentes.png)

Modo de uso: Ejecutable, entradas por consola para seleccionar las funciones matematicas.

!--- Segundo Hito ---!

1. Lo primeros que hicimos fue ver que era lo necesario para esta entrega por lo cual fuimos viendo y aprendiendo durante las clases, tambien nos dividimos la tareas de manera homogenea para que todos hicieran algo y que uno no quedara con más cosas y asi fue como pasaron las primeras semanas del proyecto.

1.  Al distribuirnos las tareas fue para que la entrega del proyecto no nos fueran tan repentina, lo cual nos dimos cuenta de los fallos del anterior hito que nos describio el ayudante,asi fuimos organizando las tareas semana a semana, arreglamos el gitlab y tambien el codigo main del proyecto.

1.  Ya despues de ir  viendo como avanzaban las semanas fuimos aprendiendo qt para implementarlo al proyecto, qt es una interfaz grafica donde podremos mostra visualmente lo que hacemos con nuestras ideas en el codigo.Implementamos qt, que hicimos pusimos botones en una calculadora y que esta mostrara los numeros que uno marcaban.

1. Para la entrega final juntamos nuestras partes del proyecto tanto como codigo, qt, gitlab,redme y fuimos viendo que nos faltaba por rubrica  para asi poder contar con un buen puntaje excelente, tambien terminamos la presentacion para el video de la entrega final, como el gitlab y el codigo junto con el qt.




![fotodiagrama](DIAGRAMA_DE_COMPONENTES.png)


!--- Tercer Hito ---!

1. Empezamos armando todo en el qt y viendo como funcionaba el codigo y arreglando algunos errores, también  fuimos resolviendo preguntas con el ayudante y compañeros de curso para ver como mejor visualmente qt para poder entregar un mejor proyecto(para usar qt usamos esta aplicaion para descargarlo y poder usarlo https://www.qt.io/ ).

1. Para este momento Empezamos a ver como hacer la presentacion final para los estudiantes y destribuirnos las partes para hablar, esto fue mas para que durante el momento de presentacion no hubiera problemas al hablar.

1. esta parte del proyecto fue mas corta ya que tenimamos todo listo de las anteriores entregas y solo fuimos juntando y armando las partes para dejarlo todo en un solo, asi dejando lo mejor posible para que el dia de la presentacion todo saliera de la mejor manera posible sin dificultades, para que los alumnos tuvieran la mejor experiencia posible para que ven lo mejor de telematica.


![fotodiagrama](Diagrama_de_componentes_hito_3_.png)





